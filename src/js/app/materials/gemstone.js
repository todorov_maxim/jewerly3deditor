import sparkle5 from './../../../assets/textures/diamand/sparkle5.png';
import sparkle3 from './../../../assets/textures/diamand/sparkle3.png';
import noiseTexture from './../../../assets/textures/diamand/noiseTexture.jpg';
import IBL_Info from './../../../assets/textures/diamand/IBL_Info.txt';
import mips from './../../../assets/textures/diamand/mips.png';


export default class GemstoneMaterial {
    constructor(viewer) {
        this.viewer = viewer;
    }

    hexToRgb(hex) {
        let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    loadgem(obj, container, color, key) {
        // hex to rgba
        let colorValue = color.replace('#', '');
        let rgba = this.hexToRgb(colorValue);
        let new_color = {
            x: 1.0 - rgba.r / 255,
            y: 1.0 - rgba.g / 255,
            z: 1.0 - rgba.b / 255
        };

        let check = true;
        let bObjectLoaded = false;
        let IsIBLDataRead = false;
        let bRingLoaded = false;

        let rootNode = new THREE.Object3D();
        rootNode.position.y = 2.1;
        this.viewer.scene.add(rootNode);

        let sparkleTexture = THREE.ImageUtils.loadTexture(sparkle5);
        let sparkleTexture1 = THREE.ImageUtils.loadTexture(sparkle3);
        let noiseTexture = THREE.ImageUtils.loadTexture(noiseTexture);
        let sparkle1 = new Sparkle(sparkleTexture, noiseTexture);
        let sparkle2 = new Sparkle(sparkleTexture1, noiseTexture);

        let diamondArray = [];
        let diamondsArray = [];


        let onReadyCallBack = () => {
            sparkle1.material.uniforms["screenTexture"].value = this.viewer.composer.renderTarget2;
            sparkle2.material.uniforms["screenTexture"].value = this.viewer.composer.renderTarget2;
            bObjectLoaded = true;
        };

        let onIBLInfoReadyCallBack = (roughnessArray, uniformTexCoordSetArray) => {
            DiamondShader.material.uniforms['TextureCoordSetArray'].value = uniformTexCoordSetArray;
            DiamondShader.material.uniforms['RoughnessArray'].value = roughnessArray;
            if (check)
                IsIBLDataRead = true;
        };

        let envTextureLoader = new HDREnvTextureLoader();
        envTextureLoader.loadIBLInfo(IBL_Info, onIBLInfoReadyCallBack);
        let envTexture = envTextureLoader.load(mips);
        envTexture.generateMipmaps = false;
        envTexture.magFilter = THREE.LinearFilter;
        envTexture.minFilter = THREE.LinearFilter;
        let diamond;
        let loadDiamond = () => {
            let diamondLoader = new DiamondLoader();
            diamondLoader.load(obj, obj3d => {
                obj3d.children.slice().forEach(mesh => {
                    mesh.geometry.computeBoundingSphere();
                    const pos = mesh.geometry.boundingSphere.center.clone();
                    const diamondProto = new Diamond(mesh, envTexture, onReadyCallBack);
                    const diamond = diamondProto.shallowCopy();
                    // diamondsArray.push(diamond);
                    diamond.mesh.position.copy(pos);
                    // diamond.cubeCamera.position.copy(pos);
                    diamond.mesh.geometry.computeBoundingSphere();
                    diamond.mesh.material.uniforms.boundingSphereRadius.value = diamond.mesh.geometry.boundingSphere.radius;
                    diamond.mesh.material.uniforms.boundingSphereCenter.value = diamond.mesh.geometry.boundingSphere.center.clone();
                    diamond.mesh.material.uniforms.Absorbption.value.x = new_color.x;
                    diamond.mesh.material.uniforms.Absorbption.value.y = new_color.y;
                    diamond.mesh.material.uniforms.Absorbption.value.z = new_color.z;

                    diamond.mesh.key = key;

                    container.add(diamond.mesh);


                    // rootNode.add(diamond.mesh);
                    // diamondArray.push(diamond);
                    this.viewer.diamondArray.push(diamond);
                    this.viewer.diamondPrototypeArray.push(diamondProto);
                });
                onRing1Loaded();
            });
        }

        loadDiamond();
        let i = 0;
        let onRing1Loaded = () => {
            i++;


            bRingLoaded = true;

            if (check) {
                this.viewer.bObjectLoaded = bObjectLoaded;
                this.viewer.IsIBLDataRead = IsIBLDataRead;
                this.viewer.bRingLoaded = bRingLoaded;
            }


        }
    }
}



