import diamond from '../../../../../../WebstormProjects/rings/frontend/web/ring/darling.png';

export default class Decals {
    constructor(viewer, mesh) {
        this.viewer = viewer;
        this.mesh = mesh;

        this.intersection = {
            intersects: false,
            point: new THREE.Vector3(),
            normal: new THREE.Vector3()
        };
        this.mouse = new THREE.Vector2();
        this.textureLoader = new THREE.TextureLoader();
        this.decalDiffuse = this.textureLoader.load(diamond);

        this.decalMaterial = new THREE.MeshPhongMaterial({
            specular: 0x444444,
            map: this.decalDiffuse,
            normalScale: new THREE.Vector2(1, 1),
            shininess: 30,
            transparent: true,
            depthTest: true,
            depthWrite: false,
            polygonOffset: true,
            polygonOffsetFactor: -4,
            wireframe: false
        });

        this.decals = [];
        this.mouseHelper = false;
        this.position = new THREE.Vector3();
        this.orientation = new THREE.Euler();
        this.size = new THREE.Vector3(10, 1.5, 1.5);


        this.init();
    }

    init() {

        this.raycaster = new THREE.Raycaster();

        let geometry = new THREE.BufferGeometry();
        geometry.setFromPoints( [ new THREE.Vector3(), new THREE.Vector3() ] );
        this.line = new THREE.Line( geometry, new THREE.LineBasicMaterial() );
        this.viewer.scene.add( this.line );

        this.mouseHelper = new THREE.Mesh( new THREE.BoxBufferGeometry( 1, 1, 10 ), new THREE.MeshNormalMaterial() );
        this.mouseHelper.visible = false;
        this.viewer.scene.add( this.mouseHelper );

        this.moved = false;
        this.viewer.controls.addEventListener('change', () => {
            this.moved = true;
        });
        window.addEventListener('mousedown', () => {
            this.moved = false;
        }, false);
        window.addEventListener('mouseup', () => {
            this.checkIntersection();
            if (!this.moved && this.intersection.intersects) this.shoot();
        });
        window.addEventListener('mousemove', (e)=>this.onTouchMove(e));
        window.addEventListener('touchmove', this.onTouchMove);
    }

    onTouchMove( event ) {
        var x, y;
        if ( event.changedTouches ) {
            x = event.changedTouches[ 0 ].pageX;
            y = event.changedTouches[ 0 ].pageY;
        } else {
            x = event.clientX;
            y = event.clientY;
        }
        this.mouse.x = ( x / window.innerWidth ) * 2 - 1;
        this.mouse.y = - ( y / window.innerHeight ) * 2 + 1;
        this.checkIntersection();
    }

    checkIntersection() {
        if ( ! this.mesh ) return;
        this.raycaster.setFromCamera( this.mouse, this.viewer.camera );
        let intersects = this.raycaster.intersectObjects( [ this.mesh ] );
        if ( intersects.length > 0 ) {
            let p = intersects[ 0 ].point;
            this.intersection.point.copy( p );
            let n = intersects[ 0 ].face.normal.clone();
            n.transformDirection( this.mesh.matrixWorld );
            n.multiplyScalar( 10 );
            n.add( intersects[ 0 ].point );
            this.intersection.normal.copy( intersects[ 0 ].face.normal );
            let positions = this.line.geometry.attributes.position;
            positions.setXYZ( 0, p.x, p.y, p.z );
            positions.setXYZ( 1, n.x, n.y, n.z );
            positions.needsUpdate = true;
            this.intersection.intersects = true;
        } else {
            this.intersection.intersects = false;
        }
    }

    shoot() {
        this.position.copy( this.intersection.point );
        this.orientation.copy( this.mouseHelper.rotation );
        var material = this.decalMaterial.clone();
        var m = new THREE.Mesh( new THREE.DecalGeometry( this.mesh, this.position, this.orientation, this.size ), material );
        this.decals.push( m );
        this.viewer.scene.add( m );
    }

}