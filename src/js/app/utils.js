export default class Utils {
    constructor() {
        this.textureLoader = new THREE.TextureLoader();

        if (window.location.href.indexOf(':8717') !== -1) {
            this.slug = 'testing';
            //Necklace
            //testing
        } else {
            let pathname = window.location.pathname;
            let slugTitle = pathname.split("/ring/").pop();
            this.slug = slugTitle;

            if(this.slug.indexOf('/') !== -1){
                this.slug = this.slug.split('/')[0];
            }
        }

        if (window.location.href.indexOf(':8717') !== -1) {
            this.url = 'http://yii2-starter-kit.localhost:8085';
        }else{
            this.url = '';
        }
    }

    sendMessage(email, text, theme){
        $.ajax({
            url: this.url + "/feedback/create",
            // headers: {
            //     "accept": "application/json",
            //     "Access-Control-Allow-Origin": "*"
            // },
            type: "POST",
            crossDomain: true,
            data: {
                email: email,
                text: text,
                theme: theme
            },
            dataType: "json",
            success: (response) => {
                console.log('RES', response);
            }
        });
    }

    getConfig(callback) {
        $.ajax({
            url: this.url + "/models/get-default-model",
            // headers: {
            //     "accept": "application/json",
            //     "Access-Control-Allow-Origin": "*"
            // },
            type: "POST",
            crossDomain: true,
            data: {slug: this.slug},
            dataType: "json",
            success: (response) => {
                callback(response);
            }
        });
    }

    getPrice(data, callback) {
        $.ajax({
            url: this.url + "/order/ring-price",
            // headers: {
            //     "accept": "application/json",
            //     "Access-Control-Allow-Origin": "*"
            // },
            type: "POST",
            crossDomain: true,
            data: data,
            dataType: "json",
            success: (response) => {
                callback(response.totalPrice);
            }
        });
    }

    initHex(color) {
        let colorValue = color.replace('#', '');
        let rgba = this.hexToRgb(colorValue);
        let new_color = {
            x: 1.0 - rgba.r / 255,
            y: 1.0 - rgba.g / 255,
            z: 1.0 - rgba.b / 255
        };

        return new_color;
    }

    hexToRgb(hex) {
        let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

}