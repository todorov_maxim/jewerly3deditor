export default class Loader{
    constructor(viewer, loading){
        this.viewer = viewer;
        this.loader = new THREE.OBJLoader();
        this.loading = loading;
    }

    loadModel(src){
        this.loader.load(src,(obj)=>{
            this.viewer.scene.add(obj);
            this.loading.hide();
        });
    }
}