export default class CanvasTexture {
    constructor() {
        this.canvas = document.createElement('canvas');
        this.canvas.width = 512;
        this.canvas.height = 128;
        this.context = this.canvas.getContext('2d');
        this.context.fillStyle = 'black';
        this.context.font = '52px Times New Roman'; // 134
        this.context.textAlign = "center";
        this.context.textBaseline = 'middle';
    }

    changeStyle(style){
        if(style === 'classic') {
            this.context.font = '52px Times New Roman';
        }else{
            this.context.font = 'italic 52px Times New Roman';
        }
    }

    createTexture(string) {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.fillText(string, this.canvas.width / 2, this.canvas.height / 2);
        return this.canvas;
    }
}