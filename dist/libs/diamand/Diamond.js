/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var DiamondShader = {};

DiamondShader.fragmenShaderNormalMapCapture = "varying vec2 vUv;\n\
                varying vec3 Normal;\n\
                varying vec3 objectPosition;\n\
                void main() {\n\
                        vec3 color = normalize(Normal);// + vec3(1.0);\n\
                        //color *= 0.5;\n\
                        gl_FragColor = vec4( color.x, color.y, color.z, objectPosition.z );\n\
                }";

DiamondShader.fragmenShaderDiamond = "#define PI 3.141592653589793\n\
                precision highp float;\n\
                varying vec2 vUv;\n\
                varying vec3 Normal;\n\
                varying vec3 worldNormal;\n\
                // varying mat4 NormalMatrix;\n\
                varying vec3 vecPos;\n\
                varying vec3 viewPos;\n\
                uniform samplerCube tCubeMap;\n\
                uniform samplerCube tCubeMapPosition;\n\
                uniform samplerCube tReflectionCube;\n\
                uniform sampler2D IBLTexture;\n\
                uniform sampler2D HDRTexture;\n\
                uniform vec4 TextureCoordSetArray[8];\n\
                uniform float RoughnessArray[8];\n\
                uniform float tanAngleSqCone;\n\
                uniform float coneHeight;\n\
                uniform float boundingSphereRadius;\n\
                uniform vec3 boundingSphereCenter;\n\
                uniform int maxBounces;\n\
                uniform mat4 ModelMatrix;\n\
                uniform mat4 InverseModelMatrix;\n\
                // Tweak params\n\
                uniform float n2;\n\
                uniform bool bIntersectSphere;\n\
                uniform bool bDebugBounces;\n\
                uniform float rIndexDelta;\n\
                uniform float normalOffset;\n\
                uniform float squashFactor;\n\
                uniform vec3 Absorbption;\n\
                // hdr params\n\
                uniform float YWhite;\n\
                uniform float LogAvgLum;\n\
                uniform float Key;\n\
                uniform float Saturation;\n\
                \n\
                float SchlickApproxFresenel(float a, float NdotV) {\n\
                    float schlick = pow(1.0 - abs(NdotV), 5.0);\n\
                    return a * ( 1.0 - schlick) + schlick;\n\
//                    return NdotV > 0.0 ? 1.0 : 0.3;\n\
                }\n\
                \n\
                vec4 rgbToSrgb(vec4 rgbColor){\n\
                    const float a = 0.055;\n\
                    return (1.0 + a) * pow(rgbColor, vec4(0.5)) - a;\n\
                }\n\
                \n\
                vec3 convertRGBEToRGB(vec4 rgbe) {\n\
                    float d = pow(2.0, rgbe.w*256.0 - 128.0);\n\
                    return vec3(rgbe) * d;\n\
                }\n\
                \n\
                vec3 tonemap(vec3 RGB) {\n\
                   //float Ywhite = 1e1;\n\
                   float white = YWhite*YWhite;\n\
                   float Ylum = dot(RGB ,vec3(0.2126, 0.7152, 0.0722));\n\
                   float Y = Key/LogAvgLum * Ylum ;\n\
                   float Yd = Y * ( 1.0 + Y/white)/( 1.0 + Y) ;\n\
                   return Yd * pow(RGB/Ylum ,vec3(Saturation));\n\
                }\n\
                \n\
                vec4 SampleSpecularContribution(vec4 specularColor, vec3 direction, float roughness) {\n\
                    vec4 texCoordSetLowerSampler;\n\
                    vec4 texCoordSetUpperSampler;\n\
                    float dRoughness = 0.0;\n\
                    texCoordSetLowerSampler = TextureCoordSetArray[0];\n\
                    texCoordSetUpperSampler = TextureCoordSetArray[1];\n\
                    dRoughness = (roughness - RoughnessArray[0])/(RoughnessArray[1] - RoughnessArray[0]);\n\
                    float phi_refl = atan(direction.z, direction.x);\n\
                    phi_refl = phi_refl < 0.0 ? 2.0*PI + phi_refl : phi_refl;\n\
                    phi_refl /= (2.0*PI);\n\
                    float theta_refl = (asin(direction.y) + PI * 0.5)/PI;\n\
                    theta_refl = theta_refl > 1.0 ? 1.0 : theta_refl;\n\
                    vec2 texCoordLower = vec2(texCoordSetLowerSampler.x + phi_refl * texCoordSetLowerSampler.y, texCoordSetLowerSampler.z + theta_refl * texCoordSetLowerSampler.w);\n\
                    vec2 texCoordUpper = vec2(texCoordSetUpperSampler.x + phi_refl * texCoordSetUpperSampler.y, texCoordSetUpperSampler.z + theta_refl * texCoordSetUpperSampler.w);\n\
                    vec4 rgbeLower = texture2D(IBLTexture, texCoordLower);\n\
                    vec4 rgbeUpper = texture2D(IBLTexture, texCoordUpper);\n\
                    vec3 rgbLower = tonemap(specularColor.xyz*convertRGBEToRGB(rgbeLower));\n\
                    vec3 rgbUpper = tonemap(specularColor.xyz*convertRGBEToRGB(rgbeUpper));\n\
                    return  vec4(rgbLower, 1.0)*(1.0-dRoughness) +  vec4(rgbUpper, 1.0)*dRoughness;\n\
                 }\n\
                 \n\
                 vec3 intersectCone(vec3 origin, vec3 direction) {\n\
                     float Ox = origin.x; float Oy = origin.y; float Oz = origin.z;\n\
                     float Dx = direction.x; float Dy = direction.y/squashFactor; float Dz = direction.z;\n\
                     float A = Dx*Dx + Dz*Dz - Dy*Dy*tanAngleSqCone;\n\
                     float B = 2.0*(Ox*Dx + Oz*Dz - Oy*Dy*tanAngleSqCone - Dy*coneHeight*tanAngleSqCone*0.5);\n\
                     float C = Ox*Ox + Oz*Oz - (Oy*Oy + coneHeight*coneHeight*0.25 + Oy*coneHeight)*tanAngleSqCone;\n\
                     float disc = B*B - 4.0*A*C;\n\
                     float eps = 1e-4;\n\
                     float t = -1.0;\n\
                     if(disc > eps) {\n\
                         disc = sqrt(disc);\n\
                         float t1 = (-B + disc)/A*0.5;\n\
                         float t2 = (-B - disc)/A*0.5;\n\
                         t = (t1 > t2) ? t1 : t2;\n\
                     }\n\
                     if(abs(disc) < eps)\n\
                         t = -B/A*0.5;\n\
                     float tplane = (coneHeight*0.5 - Oy)/Dy;\n\
                     t = t > tplane ? t : tplane;\n\
                     direction.y *= squashFactor;\n\
                     return vec3(origin + direction * t);\n\
                 }\n\
                 \n\
                 vec3 intersectSphere(vec3 origin, vec3 direction) {\n\
                     //origin -= boundingSphereCenter;\n\
                     direction.y /= squashFactor;\n\
//                     origin.y /= squashFactor;\n\
                     float A = dot(direction, direction);\n\
                     float B = 2.0*dot(origin, direction);\n\
                     float C = dot(origin, origin) - boundingSphereRadius * boundingSphereRadius;\n\
                     float disc = B*B - 4.0 * A * C;\n\
                     if(disc > 0.0) \n\
                     {\n\
                         disc = sqrt(disc);\n\
                         float t1 = (-B + disc)*0.5/A;\n\
                         float t2 = (-B - disc)*0.5/A;\n\
                         float t = (t1 > t2) ? t1 : t2;\n\
//                         origin.y *= squashFactor;\n\
                         direction.y *= squashFactor;\n\
                         return vec3(origin + direction * t);\n\
                     }\n\
                     return vec3(0.0);\n\
                 }\n\
                 \n\
                 vec3 debugBounces(int count) {\n\
                     vec3 color;\n\
                     if(count == 1)\n\
                         color = vec3(0.0,1.0,0.0);\n\
                     else if(count == 2)\n\
                         color = vec3(0.0,0.0,1.0);\n\
                     else if(count == 3)\n\
                         color = vec3(1.0,1.0,0.0);\n\
                     else if(count == 4)\n\
                         color = vec3(0.0,1.0,1.0);\n\
                     else\n\
                         color = vec3(0.0,0.0,0.0);\n\
                     if(count ==0)\n\
                         color = vec3(1.0,0.0,0.0);\n\
                     return color;\n\
                 }\n\
                 \n\
                 vec3 traceRay(vec3 origin, vec3 direction, vec3 normal) {\n\
                     vec3 outColor = vec3(0.0);\n\
                     // Refract ray entering the diamond\n\
                    const float n1 = 1.0;\n\
                    const float epsilon = 1e-4;\n\
                    float f0 = (n2- n1)/(n2 + n1);\n\
                    f0 *= f0;\n\
                    vec3 attenuationFactor = vec3(1.0);\n\
                    vec3 newDirection = refract(direction, normal, n1/n2);\n\
                    vec3 reflectedDirection = reflect(direction, normal);\n\
                    float fresenelReflected = SchlickApproxFresenel(f0, dot(normal, reflectedDirection));\n\
                    float fresenelRefracted = SchlickApproxFresenel(f0, dot(-normal, newDirection));\n\
                    attenuationFactor *= ( 1.0 - fresenelRefracted);\n\
                    outColor += SampleSpecularContribution(vec4(1.0), reflectedDirection, 0.0).rgb * fresenelReflected;\n\
                    const int iterCount = 6;\n\
                    int count = 0;\n\
                    newDirection = (InverseModelMatrix * vec4(newDirection, 0.0)).xyz;\n\
                    newDirection = normalize(newDirection);\n\
                    float boostFactor = 1.5;\
                    origin = (InverseModelMatrix * vec4(origin, 1.0)).xyz;\n\
                     // bounce the ray inside the diamond\n\
                     for( int i=0; i<iterCount; i++) {\n\
                         vec3 intersectedPos;\n\
                         if(bIntersectSphere)\n\
                            intersectedPos = intersectSphere(origin + vec3(epsilon), newDirection);\n\
                         else\n\
                            intersectedPos = intersectCone(origin + vec3(epsilon), newDirection);\n\
                         vec3 dist = intersectedPos - origin - boundingSphereCenter;\n\
                         vec3 d = normalize(intersectedPos - boundingSphereCenter);\n\
                         //d = (vec4(d, 0.0) * NormalMatrix).xyz;\n\
                         // Normal of the diamond\n\
                         vec3 mappedNormal = textureCube( tCubeMap, d ).xyz;\n\
                         mappedNormal.y += normalOffset;\n\
                         mappedNormal = normalize(mappedNormal);\n\
                         float r = sqrt(dot(dist, dist));\n\
                         // refract the ray at intersection\n\
                        vec3 oldDir = newDirection;\n\
                        newDirection = refract(newDirection, mappedNormal, n2/n1);\n\
                        origin = intersectedPos;\n\
                        //const vec3 c = vec3(0.01, 0.01, 0.01);\n\
                        //outColor += vec3(dot(newDirection, newDirection));\n\
                        attenuationFactor *= exp(-r*Absorbption);\n\
                        if( dot(newDirection, newDirection) == 0.0) { // Total Internal Reflection. Continue inside the diamond\n\
                             newDirection = reflect(oldDir, mappedNormal);\n\
                             if(i == iterCount-1 ) \n\
                             {\n\
                                float f1 = SchlickApproxFresenel(f0, dot(mappedNormal, -oldDir));\n\
                                vec3 d1 = (ModelMatrix * vec4(oldDir, 0.0)).xyz;\n\
                                outColor += SampleSpecularContribution(vec4(1.0), d1, 0.0).rgb * attenuationFactor * (1.0 - f1) * boostFactor;\n\
                                //outColor += ContributionFromLights(origin, oldDir);\n\
                             }\n\
                        } else { // Add the contribution from outgoing ray, and continue the reflected ray inside the diamond\n\
                            float fresnelRefractedRay = SchlickApproxFresenel(f0, dot(-mappedNormal, newDirection));\n\
                            // outgoing(refracted) ray's contribution \n\
                            vec3 d1 = (ModelMatrix * vec4(newDirection, 0.0)).xyz;\n\
                            vec3 colorG = SampleSpecularContribution(vec4(1.0), d1, 0.0).rgb* ( 1.0 - fresnelRefractedRay);\n\
                            vec3 dir1 = refract(oldDir, mappedNormal, (n2+rIndexDelta)/n1);\n\
                            vec3 dir2 = refract(oldDir, mappedNormal, (n2-rIndexDelta)/n1);\n\
                            vec3 d2 = (ModelMatrix * vec4(dir1, 0.0)).xyz;\n\
                            vec3 d3 = (ModelMatrix * vec4(dir2, 0.0)).xyz;\n\
                            vec3 colorR = SampleSpecularContribution(vec4(1.0), d2, 0.0).rgb * ( 1.0 - fresnelRefractedRay);\n\
                            vec3 colorB = SampleSpecularContribution(vec4(1.0), d3, 0.0).rgb * ( 1.0 - fresnelRefractedRay);\n\
                            outColor += vec3(colorR.r, colorG.g, colorB.b) * attenuationFactor;\n\
                            //outColor += ContributionFromLights(origin, newDirection);\n\
                            // new reflected ray inside the diamond\n\
                            newDirection = reflect(oldDir, mappedNormal);\n\
                            float fresnelReflectedRay = SchlickApproxFresenel(f0, dot(mappedNormal, newDirection));\n\
                            attenuationFactor *= fresnelReflectedRay * boostFactor;\n\
                            count++;\n\
                        }\n\
                     }\n\
                     if(bDebugBounces)\n\
                        outColor = debugBounces(count);\n\
                     return outColor;\n\
                 }\n\
                 \n\
                void main() {\n\
                        vec3 normalizedNormal = normalize(worldNormal);\n\
                        vec3 viewVector = normalize(vecPos - cameraPosition);\n\
                        vec3 color = traceRay(vecPos, viewVector, normalizedNormal);\n\
                       // vec4 color = textureCube( tCubeMap, normalizedNormal );\n\
                        //vec4 color = textureCube( tCubeMapPosition, normalizedNormal );\n\
                        gl_FragColor = vec4(color.rgb,0.50);//\n\
                }";

DiamondShader.vertexShader = "varying vec2 vUv;\n\
                   varying vec3 Normal;\n\
                   varying vec3 worldNormal;\n\
                   varying vec3 vecPos;\n\
                   varying vec3 viewPos;\n\
                   varying vec3 objectPosition;\n\
                   // varying mat4 NormalMatrix;\n\
                   void main() {\n\
                        vUv = uv;\n\
                        Normal =  normal;\n\
                        // NormalMatrix = modelMatrix;\n\
                        worldNormal = (modelMatrix * vec4(normal,0.0)).xyz;\n\
                        objectPosition = position;\n\
                        vecPos = (modelMatrix * vec4(position, 1.0 )).xyz;\n\
                        viewPos = (modelViewMatrix * vec4(position, 1.0 )).xyz;\n\
                        gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );\n\
                   }";

DiamondShader.material = new THREE.ShaderMaterial({
    uniforms: {
        "tCubeMap": {type: "t", value: null},
        "tReflectionCube": {type: "t", value: null},
        "IBLTexture": {type: "t", value: null},
        "HDRTexture": {type: "t", value: null},
        "boundingSphereRadius": {type: "f", value: 1.0},
        "boundingSphereCenter": {type: "v3", value: new THREE.Vector3(0, 0, 0)},
        "TextureCoordSetArray": {type: 'v4v', value: null},
        "RoughnessArray": {type: 'fv1', value: null},
        "maxBounces": {type: 'i', value: 1},
        "tanAngleSqCone": {type: 'f', value: 0.0},
        "coneHeight": {type: 'f', value: 0.0},
        "bIntersectSphere": {type: 'i', value: true},
        "bDebugBounces": {type: 'i', value: false},
        "rIndexDelta": {type: 'f', value: 0.01},
        "n2": {type: 'f', value: 2.4},              // 2.4   // 3
        "normalOffset": {type: 'f', value: 0.10},   // 0.10  // 0.20
        "squashFactor": {type: 'f', value: 0.95},
        "YWhite": {type: 'f', value: 10},           // 10    // 30
        "LogAvgLum": {type: 'f', value: 0.08},      // 0.08  // 0.3
        "Key": {type: 'f', value: 1.0},               //1      // 0.5
        "Saturation": {type: 'f', value: 0.45},
        "Absorbption": {type: 'v3', value: new THREE.Vector3(0.0, 0.0, 0.0)},
        "ModelMatrix": {type: 'm4', value: new THREE.Matrix4().identity()},
        "InverseModelMatrix": {type: 'm4', value: new THREE.Matrix4().identity()}
    },
    vertexShader: DiamondShader.vertexShader,
    fragmentShader: DiamondShader.fragmenShaderDiamond
});
DiamondShader.material.side = THREE.DoubleSide;

DiamondShader.normalMapCaptureMaterial = new THREE.ShaderMaterial({
    vertexShader: DiamondShader.vertexShader,
    fragmentShader: DiamondShader.fragmenShaderNormalMapCapture
});
DiamondShader.normalMapCaptureMaterial.side = THREE.DoubleSide;

var diamondLoader = new DiamondLoader();

function Diamond(object, envTexture, readyCallback) {
    this.envTexture = envTexture;
    this.cubeCamera = new THREE.CubeCamera(0.01, 100, 256);
    this.localScene = new THREE.Scene();
    this.localScene.add(this.cubeCamera);
    this.material = new THREE.ShaderMaterial();
    this.material.uniforms = THREE.UniformsUtils.clone(DiamondShader.material.uniforms);
    this.material.uniforms["IBLTexture"].value = this.envTexture;
    this.material.vertexShader = DiamondShader.material.vertexShader;
    this.material.fragmentShader = DiamondShader.material.fragmentShader;
    this.cubeCamera.renderTarget.generateMipmaps = false;
    this.cubeCamera.renderTarget.magFilter = THREE.NearestFilter;
    this.cubeCamera.renderTarget.minFilter = THREE.NearestFilter;
    this.cubeCamera.renderTarget.format = THREE.RGBAFormat;
    this.cubeCamera.renderTarget.type = THREE.HalfFloatType;
    this.geometry = null;
    this.mesh = null;
    this.normalBakeHelperMesh = null;
    this.position = new THREE.Vector3();
    this.rotation = new THREE.Vector3();
    this.scale = new THREE.Vector3(1, 1, 1);
    // var onObjectLoad = function (object) {
      if(object){
          this.mesh = object;
          this.normalBakeHelperMesh = this.mesh.clone();
          this.normalBakeHelperMesh.material = DiamondShader.normalMapCaptureMaterial;
          this.geometry = object.geometry;

          window.first = this.geometry.clone();
          this.mesh.traverse(function (child) {
              if (child instanceof THREE.Mesh) {
                  child.geometry.center();
                  //child.geometry.normalize();
                  child.geometry.computeBoundingSphere();


                  // let geometry = new THREE.Geometry();
                  // geometry.fromBufferGeometry(child.geometry);
                  // geometry.mergeVertices();
                  // geometry.computeBoundingBox();
                  //
                  // child.geometry = geometry;

                  window.second = child.geometry;
              }
          });
          this.localScene.add(this.normalBakeHelperMesh);
          readyCallback();
      }
    // };
    this.sparkles = [];
    // if (fileName !== undefined)
    //     diamondLoader.load(fileName, onObjectLoad.bind(this));
}

Diamond.prototype = {
    constructor: Diamond,
    // Copies the diamond with the shared geometry. If you only need to translate/rotate/scale the diamond node, use this.
    shallowCopy: function () {
        var diamond = new Diamond();
        diamond.material = new THREE.ShaderMaterial();
        diamond.material.uniforms = THREE.UniformsUtils.clone(this.material.uniforms);
        diamond.material.uniforms["tCubeMap"].value = this.cubeCamera.renderTarget.texture;
        diamond.material.uniforms["IBLTexture"].value = this.envTexture;
        diamond.material.vertexShader = this.material.vertexShader;
        diamond.material.fragmentShader = this.material.fragmentShader;
        diamond.mesh = new THREE.Mesh(this.geometry, diamond.material);
        diamond.cubeCamera = this.cubeCamera;
        diamond.geometry = this.geometry;
        diamond.position.copy(this.position);
        diamond.rotation.copy(this.rotation);
        diamond.scale.copy(this.scale);
        for (var i = 0; i < this.sparkles.length; i++) {
            diamond.sparkles.push(this.sparkles[i].shallowCopy());
        }
        return diamond;
    },

    setPosition: function (x, y, z) {
        this.position.set(x, y, z);
        this.mesh.position.x = x;
        this.mesh.position.y = y;
        this.mesh.position.z = z;
    },

    setRotation: function (x, y, z) {
        this.rotation.set(x, y, z);
        this.mesh.rotation.x = x;
        this.mesh.rotation.y = y;
        this.mesh.rotation.z = z;
    },

    setScale: function (x, y, z) {
        this.scale.set(x, y, z);
        this.mesh.scale.x = x;
        this.mesh.scale.y = y;
        this.mesh.scale.z = z;
        for (var i = 0; i < this.sparkles.length; i++) {
            this.sparkles[i].setScale(x);
        }
    },

    applyTransform: function () {
        this.mesh.updateMatrixWorld();
        this.material.uniforms["ModelMatrix"].value.copy(this.mesh.matrixWorld);
        var m1 = this.material.uniforms["ModelMatrix"].value;
        var m2 = this.material.uniforms["InverseModelMatrix"].value;
        m2.getInverse(m1);
        for (var i = 0; i < this.sparkles.length; i++) {
            this.sparkles[i].syncWithTransform(m1);
        }
    },

    prepareNormalsCubeMap: function (renderer) {
        this.cubeCamera.updateCubeMap(renderer, this.localScene);
        this.material.uniforms["tCubeMap"].value = this.cubeCamera.renderTarget.texture;
    },

    alignSparklesWithCamera: function (camera) {
        var v = new THREE.Vector3();
        for (var i = 0; i < this.sparkles.length; i++) {
            v.copy(camera.position);
            v.sub(this.sparkles[i].mesh.position);
            v.normalize();
            var rot = v.x + v.y + v.z;
            this.sparkles[i].setRotation(rot * this.sparkles[i].rotationSpeedFactor);
            this.sparkles[i].alignWithCamera(camera);
        }
    },

    addSparkle: function (sparkle) {
        this.sparkles.push(sparkle);
    }
};