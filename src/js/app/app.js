import Viewer from '../viewer/viewer';
import ObjLoader from '../loaders/objLoad';
import RingMaterial from './materials/rings';
import GemstoneMaterial from './materials/gemstone';
import Loading from './loading/loading';
import CanvasTexture from './helpers/canvasTexuture';
import Utils from './utils';
import Hand from './hand';
import Model from './../../assets/model/temp/model.obj';

export default class App {
    constructor() {
        this.viewer = new Viewer();
        this.utils = new Utils();
        this.loading = new Loading(this.viewer.container);
        this.ringMaterial = new RingMaterial();
        this.objLoader = new ObjLoader(this.viewer, this.ringMaterial, this.loading);
        this.gemstoneMaterial = new GemstoneMaterial(this.viewer);
        this.hand = new Hand(this.viewer, this.loading, this.type);

        this.canvasTexture = new CanvasTexture();

        this.mobile = document.body.offsetWidth < 800;

        if (/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            this.mobileDevice = true;
        }

        if (this.mobileDevice) {
            const alert = document.getElementById('alert');
            const angle = screen.orientation ? screen.orientation.angle : window.orientation;
            if (angle !== 0) {
                alert.style.display = 'block';
            }

            window.addEventListener("orientationchange", function () {
                const angle = screen.orientation ? screen.orientation.angle : window.orientation;
                if (angle !== 0) {
                    alert.style.display = 'block';
                } else {
                    alert.style.display = 'none';
                }
            });
        }

        window.Clipboard = (function (window, document, navigator) {
            var textArea,
                copy;

            function isOS() {
                return navigator.userAgent.match(/ipad|iphone/i);
            }

            function createTextArea(text) {
                textArea = document.createElement('textArea');
                textArea.readOnly = true;
                textArea.value = text;
                document.body.appendChild(textArea);
            }

            function selectText() {
                var range,
                    selection;

                if (isOS()) {
                    range = document.createRange();
                    range.selectNodeContents(textArea);
                    selection = window.getSelection();
                    selection.removeAllRanges();
                    selection.addRange(range);
                    textArea.setSelectionRange(0, 999999);
                } else {
                    textArea.select();
                }
            }

            function copyToClipboard() {
                document.execCommand('copy');
                document.body.removeChild(textArea);
            }

            copy = function (text) {
                createTextArea(text);
                selectText();
                copyToClipboard();
            };

            return {
                copy: copy
            };
        })(window, document, navigator);

        //price
        this.size_guide = document.getElementById('size-guide');
        this.size_guide.addEventListener('click', () => {
            document.getElementById('size-guide-img').style.display = 'block';
            if (this.mobile) {
                document.body.style.overflow = 'auto';
                document.getElementById('left-top-block').style.display = 'none';
            }
        });
        document.getElementById('size-guide-img-close').addEventListener('click', () => {
            document.getElementById('size-guide-img').style.display = 'none';


            if (this.mobile) {
                document.body.style.overflow = 'hidden';
                document.getElementById('left-top-block').style.display = 'block';
            }
        });
        this.url_copy = document.getElementById('url_copy');
        this.priceHTML = document.getElementById('price');
        this.fingerSize = document.getElementById('finger-size');
        this.fingerSize.addEventListener('click', () => {
            let sizes = document.getElementById('sizes-slider');
            let size_block = document.getElementById('size-block');
            let price = document.getElementById('price');
            let check = document.getElementById('check-out');

            if (sizes.style.display !== 'block') {
                sizes.style.display = 'block';
                if (this.mobile) {
                    check.style.display = 'none';
                    price.style.display = 'none';
                    size_block.style.display = 'none';
                }

            } else {
                sizes.style.display = 'none'
            }

            this.sizeSlider.update();
        });
        this.adviseHTML = document.getElementById('advise');
        this.social = document.getElementById('social');
        this.social.addEventListener('click', () => document.getElementById('social-icons').style.display = 'block');
        document.getElementById('close-social-modal').addEventListener('click', () => document.getElementById('social-icons').style.display = 'none');

        this.tooltip = document.getElementById('tooltip');


        this.copy_link = document.getElementById('copy-link');
        this.copy_link.addEventListener('click', () => this.createShareLink());


        document.getElementById('show-contact-modal').addEventListener('click', () => document.getElementById('contact-us-modal').style.display = 'block');
        document.getElementById('close-contact-modal').addEventListener('click', () => document.getElementById('contact-us-modal').style.display = 'none');

        document.getElementById('contact-us-btn').addEventListener('click', () => {
            const email = document.getElementById('email').value;
            const message = document.getElementById('message').value;
            const subject = document.getElementById('subject').value;

            this.utils.sendMessage(email, message, subject);
        });

        this.sliderConfig = {
            materialSwiper: {
                slides: 5,
                next: document.getElementById('n'),
                prev: document.getElementById('p')
            },
            gemSwiper: {
                slides: 5,
                next: document.getElementById('n-g'),
                prev: document.getElementById('p-g')
            },
            partSlider: {
                slides: 4,
                next: document.getElementById('n-p'),
                prev: document.getElementById('p-p')
            },
            sizeSlider: {
                slides: 7
            }
        };

        if (this.mobile) {
            this.materialSwiper = new Swiper('.materials', {
                speed: 500,
                slidesPerView: 5,
                loop: true,
                breakpointsInverse: true,
                navigation: {
                    nextEl: '#n',
                    prevEl: '#p'
                },
                on: {
                    init: function () {
                        setTimeout(() => this.update(), 1);
                    }
                }
            });
            this.gemSwiper = new Swiper('.materials_gem', {
                speed: 500,
                slidesPerView: 5,
                loop: true,
                breakpointsInverse: true,
                navigation: {
                    nextEl: '#n-g',
                    prevEl: '#p-g'
                },
                on: {
                    init: function () {
                        setTimeout(() => this.update(), 1);
                    }
                }
            });
            this.partSlider = new Swiper('.parts', {
                speed: 500,
                slidesPerView: 4,
                direction: 'vertical',
                loop: true,
                navigation: {
                    nextEl: '#n-p',
                    prevEl: '#p-p'
                },
                on: {
                    init: function () {
                        setTimeout(() => this.update(), 1);
                    }
                },
            });

            this.materialsSlidersUpdate();
            this.partSliderUpdate();

            this.sizeSlider = new Swiper('#sizes-slider', {
                speed: 500,
                slidesPerView: 7,
                on: {
                    init: function () {
                        setTimeout(() => this.update(), 1);
                    }
                },
            });

        } else {

            this.sizeSlider = new Swiper('#sizes-slider', {
                speed: 500,
                direction: 'vertical',
                slidesPerView: 12,
                on: {
                    init: function () {
                        setTimeout(() => this.update(), 1);
                    }
                },
            });
        }

        this.inEdit = false;
        this.engraving = false;

        //materials
        this.activePart = 'active_part';

        this.utils.getConfig((response) => {
            this.data = response;
            this.type = response.type;
            this.hand.type = this.type;

            if (this.type === 3) { // necklace
                document.getElementById('hand').innerHTML = 'View on Neck';
                this.viewer.objContainer.position.y = 3.85;
            }


            this.configAdapter();
            this.init();
            this.handEvent();
            this.engravingEvent();

            const link = this.createShareLink(true);
            document.getElementById('facebook-social').addEventListener('click', () => {
                window.open('http://www.facebook.com/sharer.php?u=' + link, '_blank');
            });
            document.getElementById('twitter-social').addEventListener('click', () => {
                window.open('http://twitter.com/share?text=CLIO&url=' + link, '_blank');
            });
        });

    }

    materialsSlidersUpdate() {
        this.materialSwiper.update();
        this.gemSwiper.update();

        if (this.materialSwiper.slides.length > this.sliderConfig.materialSwiper.slides) {
            this.sliderConfig.materialSwiper.next.style.display = 'block';
            this.sliderConfig.materialSwiper.prev.style.display = 'block';
        } else {
            this.sliderConfig.materialSwiper.next.style.display = 'none';
            this.sliderConfig.materialSwiper.prev.style.display = 'none';
        }

        if (this.gemSwiper.slides.length > this.sliderConfig.gemSwiper.slides) {
            this.sliderConfig.gemSwiper.next.style.display = 'block';
            this.sliderConfig.gemSwiper.prev.style.display = 'block';
        } else {
            this.sliderConfig.gemSwiper.next.style.display = 'none';
            this.sliderConfig.gemSwiper.prev.style.display = 'none';
        }
    }

    partSliderUpdate() {
        this.partSlider.update();

        if (this.partSlider.slides.length > this.sliderConfig.partSlider.slides) {
            this.sliderConfig.partSlider.next.style.display = 'block';
            this.sliderConfig.partSlider.prev.style.display = 'block';
        } else {
            this.sliderConfig.partSlider.next.style.display = 'none';
            this.sliderConfig.partSlider.prev.style.display = 'none';
        }

    }

    engravingEvent() {
        let engraving_block = document.getElementById('engraving-block');

        let engraving = document.getElementById('engraving');
        engraving.addEventListener('click', () => {
            engraving_block.style.display = 'block';
        });

        let engraving_close = document.getElementById('engraving-close');
        engraving_close.addEventListener('click', () => {
            engraving_block.style.display = 'none';
        });

        let fonts = document.getElementsByClassName('font-type');

        for (let i = 0; i < fonts.length; i++) {
            fonts[i].addEventListener('click', (e) => {

                for (let y = 0; y < fonts.length; y++) {
                    fonts[y].classList.remove('active');
                }

                fonts[i].classList.add('active');
                this.canvasTexture.changeStyle(fonts[i].id);

            });
        }


        document.getElementById('engraving_input').addEventListener('change', (e) => {
            this.inEdit = e.currentTarget.value;
        });

        document.getElementById('engrav').addEventListener('click', () => {
            if (this.inEdit && this.inEdit.length > 0) {
                if (!this.viewer.engrav.visible)
                    this.changePrice(parseInt(document.getElementById('price').innerText.split('$')[1]) + 20);
                this.engraving = this.inEdit;
                const texture = this.canvasTexture.createTexture(this.engraving);
                const canvasImage = new THREE.CanvasTexture(texture);
                this.viewer.engrav.visible = true;
                this.viewer.engrav.material.map = canvasImage;
            } else {
                if (this.viewer.engrav.visible)
                    this.changePrice(parseInt(document.getElementById('price').innerText.split('$')[1]) - 20);

                this.viewer.engrav.visible = false;
            }

            engraving_block.style.display = 'none';
        });

    }

    changePrice(price) {
        this.priceHTML.innerHTML = 'Price: $' + price;
    }

    changeSize(size) {
        this.fingerSize.innerHTML = 'Finger size: ' + size;

        let sizes = document.getElementById('sizes-slider');
        let size_block = document.getElementById('size-block');
        let price = document.getElementById('price');
        let check = document.getElementById('check-out');
        sizes.style.display = 'none';
        check.style.display = 'block';
        price.style.display = 'block';
        size_block.style.display = 'block';
    }

    changeAdvise(advise) {
        this.adviseHTML.style.display = 'block';
        this.adviseHTML.innerHTML = advise;
        setTimeout(() => {
            this.adviseHTML.style.display = 'none';
        }, 3000);
    }

    initRaycaster() {
        this.raycaster = new THREE.Raycaster();
        this.mouseVector = new THREE.Vector2();


        this.viewer.container.addEventListener('mousemove', (event) => {
            this.mouseVector.x = (event.layerX / this.viewer.container.offsetWidth) * 2 - 1;
            this.mouseVector.y = -(event.layerY / this.viewer.container.offsetHeight) * 2 + 1;

            let raycasted = this.getIntesected();

            if (raycasted.length > 0) {
                document.body.style.cursor = 'pointer';
            } else {
                document.body.style.cursor = 'default';
            }

        });

        this.viewer.container.addEventListener('click', (event) => {
            this.mouseVector.x = (event.layerX / this.viewer.container.offsetWidth) * 2 - 1;
            this.mouseVector.y = -(event.layerY / this.viewer.container.offsetHeight) * 2 + 1;

            let raycasted = this.getIntesected();
            this.findMaterialRaycasted(raycasted);

        });

        this.viewer.container.addEventListener('touchend', (event) => {
            this.mouseVector.x = (event.changedTouches[0].clientX / this.viewer.container.offsetWidth) * 2 - 1;
            this.mouseVector.y = -(event.changedTouches[0].clientY / this.viewer.container.offsetHeight) * 2 + 1;

            let raycasted = this.getIntesected();
            this.findMaterialRaycasted(raycasted);
        });
    }

    findMaterialRaycasted(raycasted) {
        if (raycasted.length > 0 && raycasted[0].object.name !== 'hand' && raycasted[0].object.name !== 'gravirovka' && raycasted[0].object.visible) {

            this.activePart = raycasted[0].object;
            let key = raycasted[0].object.key;
            // TO find materials
            for (let i = 0; i < this.config[key].length; i++) {
                if (this.config[key][i].selected) {
                    // if (raycasted[0].object.name === 'part') {
                    //     this.showMaterials(this.config[key][i].metals_variation, 'metal');
                    // } else {
                    //     this.showMaterials(this.config[key][i].stone.stones_type, 'diamond');
                    // }


                    this.showMaterials(this.config[key][i].metals_variation, 'metal');
                    if (!Array.isArray(this.config[key][i].stone)) {
                        this.showMaterials(this.config[key][i].stone.stones_type, 'diamond');
                    } else {
                        let container = document.getElementById('material_container_gem');
                        container.innerHTML = "";
                        if (this.mobile)
                            this.materialsSlidersUpdate();
                    }
                }
            }
            // END - TO find materials

            // TO show parts
            this.showParts(key);
            // END - TO show parts
        }
    }

    getIntesected() {
        this.raycaster.setFromCamera(this.mouseVector, this.viewer.camera);
        let raycasted = this.raycaster.intersectObjects(this.viewer.scene.children, true);
        return raycasted;
    }

    configAdapter() {
        let config_parts = {};
        for (let key in this.data.parts) {
            config_parts[this.data.parts[key].title_part] = [];
            for (let id in this.data.parts[key].variations) {

                if (this.data.parts[key].variations[id].url_model.indexOf('file-storage/view') !== -1 && this.data.parts[key].variations[id].metals_variation.length === 0) {
                    this.data.parts[key].variations[id].url_model = Model;
                    let temp_obj = {
                        99999: {
                            "metal_icon": "/",
                            "metal_texture": "/",
                            "metal_variant_part_ring_id": 99999,
                            "selected": true,
                            "title_metal": "No material"
                        }
                    };
                    this.data.parts[key].variations[id].metals_variation = temp_obj;

                }


                config_parts[this.data.parts[key].title_part].push(this.data.parts[key].variations[id]);
            }
        }

        this.objLoader.neededParts = Object.keys(config_parts).length;
        this.config = config_parts;

        this.first_cofig = this.createConfig('share-link');

        this.checkSharedLink();

        this.createRing(this.config);
    }

    checkSharedLink() {

        if (window.location.href.indexOf('?ring_id') !== -1) {

            let unparsedStr = window.location.href.split('?')[1];
            let unparsedArr = unparsedStr.split('&');

            //check ring_id
            if (this.data.id !== parseInt(unparsedArr[0].split('=')[1])) {
                return false;
            }
            //check ring_id --- end

            // size
            let size_id = parseInt(unparsedArr[1].split('=')[1]);

            for (let i = 0; i < this.data.sizes.length; i++) {
                this.data.sizes[i].selected = false;
                if (this.data.sizes[i].size_id == size_id) {
                    this.data.sizes[i].selected = true;
                }
            }
            // size -end

            //engraving
            let engraving = unparsedArr[2].split('=')[1];

            if (engraving !== 'false') {
                if (engraving.indexOf('_') !== -1)
                    engraving = engraving.replace(/_/g, ' ');

                if (!this.viewer.engrav.visible)
                    this.preloadedPrice = true;
                this.inEdit = engraving;
                this.engraving = this.inEdit;
                const texture = this.canvasTexture.createTexture(this.engraving);
                const canvasImage = new THREE.CanvasTexture(texture);
                this.viewer.engrav.visible = true;
                this.viewer.engrav.material.map = canvasImage;

                document.getElementById('engraving_input').value = engraving;

            }
            // engraving - end


            // parts
            for (let key in this.config) {
                for (let i = 0; i < unparsedArr.length; i++) {
                    if (unparsedArr[i].indexOf('%20') !== -1) {
                        unparsedArr[i] = unparsedArr[i].replace(/%20/g, ' ');
                    }
                    if (unparsedArr[i].indexOf(key) !== -1) { /// to Lower case
                        if (!unparsedArr[i].indexOf('stone') !== -1) {
                            let selectedId = parseInt(unparsedArr[i].split('=')[1]);

                            for (let y = 0; y < this.config[key].length; y++) {

                                this.config[key][y].selected = false;

                                for (let mat_key in this.config[key][y].metals_variation) {
                                    this.config[key][y].metals_variation[mat_key].selected = false;
                                    if (this.config[key][y].metals_variation[mat_key].metal_variant_part_ring_id == selectedId) {
                                        this.config[key][y].selected = true;
                                        this.config[key][y].metals_variation[mat_key].selected = true;
                                    }
                                }

                            }

                        } else {
                            let materialId = parseInt(unparsedArr[i].split('=')[1]);
                            let stoneId = parseInt(unparsedArr[i].split('=')[2]);


                            for (let y = 0; y < this.config[key].length; y++) {

                                this.config[key][y].selected = false;

                                for (let mat_key in this.config[key][y].metals_variation) {
                                    this.config[key][y].metals_variation[mat_key].selected = false;
                                    if (this.config[key][y].metals_variation[mat_key].metal_variant_part_ring_id == materialId) {
                                        this.config[key][y].selected = true;
                                        this.config[key][y].metals_variation[mat_key].selected = true;


                                        // search for gem

                                        for (let gem_key in  this.config[key][y].stone.stones_type) {
                                            this.config[key][y].stone.stones_type[gem_key].selected = false;

                                            if (this.config[key][y].stone.stones_type[gem_key].type_stone_variant_part_ring_id == stoneId) {
                                                this.config[key][y].stone.stones_type[gem_key].selected = true;
                                            }

                                        }

                                    }
                                }

                            }


                        }
                    }
                }
            }
            // parts - end


            //change_price
            this.createPriceConfig();
            //change_price - end


            return true;
        }

        return false;
    }

    createShareLink(checkout) {
        if (!checkout) {
            this.url_copy.style.display = 'block';
            setTimeout(() => {
                this.url_copy.style.display = 'none';
            }, 400);
        }

        const config = this.createConfig('share-link');

        const different = this.checkForDifference(config);

        if (!different) {
            if (!checkout) {
                window.Clipboard.copy(window.location.href.split('?')[0]);
                // this.copyToClipboard();
            } else
                return window.location.href;
        } else {

            let engraving_text = config.OrderItem.engraving_text;
            if (engraving_text && engraving_text.indexOf(' ') !== -1) {
                engraving_text = engraving_text.replace(/ /g, '_');
            }

            let original_url = window.location.href.indexOf('?ring_id') !== -1 ? window.location.href.split('?ring_id')[0] : window.location.href;

            let url = original_url + '?ring_id=' + config.OrderItem.ring_id + '&size_id=' + config.OrderItem.size_id + '&engraving=' + engraving_text;

            for (let i = 0; i < config.OrderItemPart.length; i++) {
                let partname = config.OrderItemPart[i].part;
                if (partname.indexOf(' ') !== -1) {
                    partname = partname.replace(/ /g, '%20');
                }

                url += '&' + partname + '=' + config.OrderItemPart[i].metal_variant_part_ring_id;
                if (config.OrderItemPart[i].type_stone_variant_part_ring_id) {
                    url += 'stone=' + config.OrderItemPart[i].type_stone_variant_part_ring_id;
                }
            }

            if (!checkout) {
                window.Clipboard.copy(url);
                // this.copyToClipboard(url);
            } else
                return url;
        }

    }

    copyToClipboard(data) {
        const el = document.createElement('textarea');

        el.value = data.trim();
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);

        const selected =
            document.getSelection().rangeCount > 0 ? document.getSelection().getRangeAt(0) : false;

        el.select();

        document.execCommand('copy');
        document.body.removeChild(el);

        if (selected) {
            document.getSelection().removeAllRanges();
            document.getSelection().addRange(selected);
        }
    }

    checkForDifference(config) {

        // check for difference
        if (!this.viewer.engrav.visible) {
            config.OrderItem.engraving_text = false;
        }

        if (config.OrderItem.engraving_text !== this.first_cofig.OrderItem.engraving_text)
            return true;

        if (config.OrderItem.size_id !== this.first_cofig.OrderItem.size_id)
            return true;

        for (let i = 0; i < config.OrderItemPart.length; i++) {
            if (config.OrderItemPart[i].metal_variant_part_ring_id !== this.first_cofig.OrderItemPart[i].metal_variant_part_ring_id) {
                return true;
            }

            if (config.OrderItemPart[i].type_stone_variant_part_ring_id) {
                if (config.OrderItemPart[i].type_stone_variant_part_ring_id !== this.first_cofig.OrderItemPart[i].type_stone_variant_part_ring_id) {
                    return true;
                }
            }
        }
        // check for difference - END

        return false;
    }

    showEngraving(type) {
        let engraving = document.getElementById('engraving');
        if (type)
            engraving.style.display = 'block';
        else
            engraving.style.display = 'none';
    }

    createRing(config_parts) {
        for (let key in config_parts) {
            for (let i = 0; i < config_parts[key].length; i++) {
                let part = config_parts[key][i];
                if (part.selected) {
                    let object3D = new THREE.Object3D();
                    object3D.name = key;
                    this.viewer.objContainer.add(object3D);

                    if (part.engraving) {
                        this.showEngraving(true);
                    }

                    if (part.url_model !== 'http://yii2-starter-kit.localhost:8085/file-storage/view') {
                        for (let mat_key in part.metals_variation) {
                            if (part.metals_variation[mat_key].selected) {
                                let material = part.metals_variation[mat_key].metal_texture;
                                this.objLoader.loadPart(part.url_model, object3D, material, key);
                            }
                        }
                    } else {
                        this.objLoader.loadNull();
                    }


                    if (part.stone.url_model_stone) {
                        for (let stone_key in part.stone.stones_type) {
                            if (part.stone.stones_type[stone_key].selected) {
                                this.gemstoneMaterial.loadgem(part.stone.url_model_stone, object3D, part.stone.stones_type[stone_key].color, key);
                            }
                        }

                    }


                }
            }
        }


        const engrav = this.checkForEngrav();
        if (engrav) {
            this.showEngraving(true);
        } else {
            this.showEngraving(false);
            this.viewer.engrav.visible = false;
        }
    }

    showMaterials(config, type) {
        // let container = document.getElementById('material_container');
        // container.innerHTML = "";

        if (type === 'metal') {
            let container = document.getElementById('material_container');
            container.innerHTML = "";

            if (config[99999])
                return;

            for (let key in config) {
                let material_div = document.createElement('div');
                material_div.className = this.mobile ? 'swiper-slide material' : 'material';

                let img = document.createElement('img');
                img.src = config[key].metal_icon;

                let span = document.createElement('span');
                span.innerHTML = config[key].title_metal;


                material_div.appendChild(img);


                if (config[key].selected) {
                    img.parentElement.classList.add('active');
                }
                //material_div.appendChild(span);

                if (!this.mobile) {
                    material_div.addEventListener('mouseover', (event) => {
                        this.tooltip.innerHTML = config[key].title_metal;
                        this.tooltip.classList.add('show-tool');
                        this.tooltip.style.transform = `translate(${event.x}px,${event.y - 100}px)`;
                    });

                    material_div.addEventListener('mouseout', (event) => {
                        this.tooltip.classList.remove('show-tool');
                    });
                }


                material_div.addEventListener('click', () => {
                    this.changeMaterial(config[key].metal_texture, 'metal');
                    this.changeAdvise(config[key].title_metal);

                    for (let key2 in config) {
                        config[key2].selected = false;

                        if (key === key2) {
                            config[key2].selected = true;
                        }
                    }
                    this.showMaterials(config, 'metal');

                    this.createPriceConfig();
                });

                container.appendChild(material_div);
            }
        } else {
            let container = document.getElementById('material_container_gem');
            container.innerHTML = "";
            for (let key in config) {
                let material_div = document.createElement('div');
                material_div.className = this.mobile ? 'swiper-slide material' : 'material';

                let img = document.createElement('img');
                img.src = config[key].url_stone_icon;


                let span = document.createElement('span');
                span.innerHTML = config[key].title;


                material_div.appendChild(img);
                if (config[key].selected) {
                    img.parentElement.classList.add('active');
                }
                // material_div.appendChild(span);


                if (!this.mobile) {
                    material_div.addEventListener('mouseover', (event) => {
                        this.tooltip.innerHTML = config[key].title;
                        this.tooltip.classList.add('show-tool');
                        this.tooltip.style.transform = `translate(${event.x}px,${event.y - 100}px)`;
                    });

                    material_div.addEventListener('mouseout', (event) => {
                        this.tooltip.classList.remove('show-tool');
                    });
                }

                material_div.addEventListener('click', () => {
                    this.changeMaterial(config[key].color, 'diamond');
                    this.changeAdvise(config[key].title);

                    document.getElementById('gem-description').innerText = config[key].description;
                    document.getElementById('gem-description').style.display = 'block';

                    setTimeout(() => {
                        document.getElementById('gem-description').style.display = 'none';
                    }, 3000);


                    for (let key2 in config) {
                        config[key2].selected = false;

                        if (key === key2) {
                            config[key2].selected = true;
                        }
                    }
                    this.showMaterials(config, 'diamond');

                    this.createPriceConfig();
                });

                container.appendChild(material_div);
            }
        }

        if (this.mobile)
            this.materialsSlidersUpdate();
    }

    changeMaterial(texture, type) {
        if (type === 'metal') {
            this.loading.show();
            this.ringMaterial.createMaterial(texture, (material) => {
                if (this.activePart.name === 'part') {
                    this.activePart.material = material;
                } else {
                    this.getPartForMaterialChanging();
                    this.activePart.material = material;
                }
                // this.activePart.material = material;
                this.loading.hide();
            });
        } else {
            let color = this.utils.initHex(texture);
            for (let i = 0; i < this.activePart.parent.children.length; i++) {
                if (this.activePart.parent.children[i].name !== 'part') {
                    this.activePart.parent.children[i].material.uniforms.Absorbption.value.x = color.x;
                    this.activePart.parent.children[i].material.uniforms.Absorbption.value.y = color.y;
                    this.activePart.parent.children[i].material.uniforms.Absorbption.value.z = color.z;
                }
            }


        }
    }

    getPartForMaterialChanging() {
        for (let i = 0; i < this.activePart.parent.children.length; i++) {
            if (this.activePart.parent.children[i].name === 'part') {
                this.activePart = this.activePart.parent.children[i];
                return;
            }
        }
    }

    showParts(key) {
        let container = document.getElementById('parts_container');
        container.innerHTML = "";

        for (let i = 0; i < this.config[key].length; i++) {
            let material_div = document.createElement('div');
            material_div.className = this.mobile ? 'swiper-slide part' : 'part';

            let img = document.createElement('img');
            img.src = this.config[key][i].url_image;

            let span = document.createElement('span');
            span.innerHTML = this.config[key][i].title_variation;


            material_div.appendChild(img);
            if (this.config[key][i].selected) {
                img.parentElement.classList.add('active');
            }
            //material_div.appendChild(span);

            material_div.addEventListener('click', () => {

                // for (let y = 0; y < this.config[key].length; y++) {
                //     this.config[key][y].selected = false;
                //     if (i === y) {
                //         this.config[key][y].selected = true;
                //     }
                // }

                this.changeAdvise(this.config[key][i].title_variation);
                this.changePart(this.config[key][i], i);
                this.showParts(key);

                this.showMaterials(this.config[key][i].metals_variation, 'metal');
                if (!Array.isArray(this.config[key][i].stone)) {
                    this.showMaterials(this.config[key][i].stone.stones_type, 'diamond');
                } else {
                    let container = document.getElementById('material_container_gem');
                    container.innerHTML = "";
                }


                this.createPriceConfig();
            });

            container.appendChild(material_div);
        }

        if (this.mobile)
            this.partSliderUpdate();
    }

    changePart(config, index) {
        let container = this.activePart.parent;
        let key = this.activePart.parent.name;

        let prev_material_title = false;
        let prev_stone_title = false;

        for (let y = 0; y < this.config[key].length; y++) {
            if (this.config[key][y].selected) {

                if (this.config[key][y].stone.url_model_stone) {
                    for (let stone_key in this.config[key][y].stone.stones_type) {
                        if (this.config[key][y].stone.stones_type[stone_key].selected) {
                            prev_stone_title = this.config[key][y].stone.stones_type[stone_key].title;
                        }
                    }
                }

                for (let mat_key in this.config[key][y].metals_variation) {
                    if (this.config[key][y].metals_variation[mat_key].selected) {
                        prev_material_title = this.config[key][y].metals_variation[mat_key].title_metal;
                    }
                }
            }
        }

        for (let y = 0; y < this.config[key].length; y++) {
            this.config[key][y].selected = false;
            if (index === y) {
                this.config[key][y].selected = true;

                for (let mat_key in this.config[key][y].metals_variation) {
                    if (prev_material_title && prev_material_title === this.config[key][y].metals_variation[mat_key].title_metal) {
                        for (let mat_key1 in this.config[key][y].metals_variation) {
                            this.config[key][y].metals_variation[mat_key1].selected = false;
                        }
                        this.config[key][y].metals_variation[mat_key].selected = true;
                    }
                }


                if (this.config[key][y].stone.url_model_stone) {
                    for (let stone_key in this.config[key][y].stone.stones_type) {
                        if (prev_stone_title && prev_stone_title === this.config[key][y].stone.stones_type[stone_key].title) {
                            for (let stone_key1 in this.config[key][y].stone.stones_type) {
                                this.config[key][y].stone.stones_type[stone_key1].selected = false;
                            }
                            this.config[key][y].stone.stones_type[stone_key].selected = true;
                        }
                    }
                }

            }
        }


        // clear all
        let length = container.children.length;
        for (let i = length - 1; i >= 0; i--) {
            container.children[i].material.dispose();
            container.children[i].geometry.dispose();
            container.remove(container.children[i]);
        }
        // END - clear ALl


        let texture = 'none';
        for (let key in config.metals_variation) {
            if (config.metals_variation[key].selected) {
                texture = config.metals_variation[key].metal_texture;
            }
        }

        this.loading.show();
        this.objLoader.loadPart(config.url_model, container, texture, key);

        if (config.stone.url_model_stone) {
            for (let stone_key in config.stone.stones_type) {
                if (config.stone.stones_type[stone_key].selected) {
                    this.gemstoneMaterial.loadgem(config.stone.url_model_stone, container, config.stone.stones_type[stone_key].color, key);
                }
            }

        }


        const engrav = this.checkForEngrav();
        if (engrav) {
            this.showEngraving(true);
        } else {
            this.showEngraving(false);
            this.viewer.engrav.visible = false;
        }


        this.setNewActivePart(config.metals_variation, key, container);
    }

    setNewActivePart(metals, key, container) {
        let interv = setInterval(() => {
            if (container.children.length > 0) {
                for (let i = 0; i < container.children.length; i++) {
                    if (container.children[i].name === 'part') {
                        this.showParts(key);
                        this.showMaterials(metals, 'metal');
                        this.activePart = container.children[i];
                        clearInterval(interv);
                    }
                }
            }
        }, 800);
    }

    createConfig(type) {
        let engraving = '';
        for (let key in this.config) {
            for (let i = 0; i < this.config[key].length; i++) {
                if (this.config[key][i].selected && this.config[key][i].engraving == 1 && this.engraving && this.viewer.engrav.visible) {
                    engraving = this.engraving;
                }
            }
        }

        let price_congif;
        if (this.type !== 3) {
            let size_id = 'undef';
            for (let i = 0; i < this.data.sizes.length; i++) {
                if (this.data.sizes[i].selected) {
                    size_id = this.data.sizes[i].size_id;
                }
            }

            price_congif = {
                "OrderItem": {
                    'ring_id': this.data.id,
                    'size_id': size_id,
                    'engraving': engraving
                },
                'OrderItemPart': []
            };
        } else {
            price_congif = {
                "OrderItem": {
                    'ring_id': this.data.id,
                    'engraving': engraving
                },
                'OrderItemPart': []
            };
        }


        for (let key in this.config) {
            for (let i = 0; i < this.config[key].length; i++) {
                if (this.config[key][i].selected) {

                    if (type === 'check') {
                        let metal_id = 'no_id';
                        let diamond_id = 'no_id';

                        if (!this.config[key][i].metals_variation[99999]) {
                            for (let key2 in this.config[key][i].metals_variation) {
                                if (this.config[key][i].metals_variation[key2].selected) {
                                    metal_id = this.config[key][i].metals_variation[key2].metal_variant_part_ring_id;
                                }
                            }
                        }

                        if (this.config[key][i].stone.url_model_stone) {
                            for (let key2 in this.config[key][i].stone.stones_type) {
                                if (this.config[key][i].stone.stones_type[key2].selected) {
                                    diamond_id = this.config[key][i].stone.stones_type[key2].type_stone_variant_part_ring_id;
                                }
                            }
                        }

                        if (diamond_id === 'no_id') {
                            price_congif.OrderItemPart.push(
                                {
                                    'metal_variant_part_ring_id': metal_id
                                }
                            );
                        } else if (metal_id === 'no_id') {
                            price_congif.OrderItemPart.push(
                                {
                                    'type_stone_variant_part_ring_id': diamond_id
                                }
                            );
                        } else {
                            price_congif.OrderItemPart.push(
                                {
                                    'metal_variant_part_ring_id': metal_id,
                                    'type_stone_variant_part_ring_id': diamond_id
                                }
                            );
                        }

                    } else if (type === 'share-link') {

                        let metal_id = 'no_id';
                        let diamond_id = 'no_id';
                        let part_name;


                        for (let key2 in this.config[key][i].metals_variation) {
                            if (this.config[key][i].metals_variation[key2].selected) {
                                metal_id = this.config[key][i].metals_variation[key2].metal_variant_part_ring_id;
                                part_name = key;
                            }
                        }

                        if (this.config[key][i].stone.url_model_stone) {
                            for (let key2 in this.config[key][i].stone.stones_type) {
                                if (this.config[key][i].stone.stones_type[key2].selected) {
                                    diamond_id = this.config[key][i].stone.stones_type[key2].type_stone_variant_part_ring_id;
                                }
                            }
                        }

                        if (diamond_id === 'no_id') {
                            price_congif.OrderItemPart.push(
                                {
                                    'metal_variant_part_ring_id': metal_id,
                                    'part': part_name
                                }
                            );
                        } else {
                            price_congif.OrderItemPart.push(
                                {
                                    'metal_variant_part_ring_id': metal_id,
                                    'type_stone_variant_part_ring_id': diamond_id,
                                    'part': part_name
                                }
                            );
                        }

                        price_congif.OrderItem.engraving_text = this.engraving;

                    }


                }
            }
        }

        return price_congif;
    }

    checkForEngrav() {
        for (let key in this.config) {
            for (let i = 0; i < this.config[key].length; i++) {
                if (this.config[key][i].selected && this.config[key][i].engraving == 1) {
                    return true;
                }
            }
        }

        return false;
    }

    createPriceConfig() {
        let price_config = this.createConfig('check');

        this.utils.getPrice(price_config, (price) => {
            this.changePrice(price);
        });
    }

    showSizes(data) {
        let container = document.getElementById('sizes');
        container.innerHTML = '';
        for (let i = 0; i < data.length; i++) {
            let div = document.createElement('div');
            div.className = 'swiper-slide';
            let span = document.createElement('span');
            div.appendChild(span);
            span.innerHTML = parseFloat(data[i].size);
            if (data[i].selected) {
                span.className = 'active';
                this.changeSize(parseFloat(data[i].size));
            }
            container.appendChild(div);


            span.addEventListener('click', () => {
                for (let y = 0; y < data.length; y++) {
                    data[y].selected = false;

                    if (data[y] === data[i]) {
                        data[y].selected = true;
                    }

                    this.showSizes(data);
                }
            });
        }

        this.sizeSlider.update();
    }

    handEvent() {
        const hand = document.getElementById('hand');
        const exitMode = document.getElementById('handModeExit');

        hand.addEventListener('click', () => {
            document.getElementsByTagName('canvas')[0].style.zIndex = 1;
            if (!this.mobile) {
                document.getElementsByTagName('canvas')[0].style.left = 0;
            }
            exitMode.style.display = 'block';
            this.hand.showHand();
        });

        exitMode.addEventListener('click', () => {
            document.getElementsByTagName('canvas')[0].style.zIndex = 0;
            if (!this.mobile) {
                document.getElementsByTagName('canvas')[0].style.left = '-15%';
            }
            exitMode.style.display = 'none';
            this.hand.hideHand();
        });

    }

    orderEvent() {
        document.getElementById('check-out').addEventListener('click', () => {
            this.createFormForOrder();
        });
    }

    createFormForOrder() {
        let config = this.createConfig('check');
        let form_container = document.getElementById('order-form');

        // create link foк coming back
        let link = this.createShareLink(true);
        let input = document.createElement('input');
        input.name = 'back_to_ring_link';
        input.value = link;
        form_container.appendChild(input);

        for (let key in config) {
            if (key !== 'OrderItemPart') {
                for (let key2 in config[key]) {
                    let input = document.createElement('input');
                    let name = key + '[' + key2 + ']';
                    input.name = name;
                    input.value = config[key][key2];

                    form_container.appendChild(input);
                }
            } else {
                for (let i = 0; i < config[key].length; i++) {

                    let input, name;

                    if (config[key][i].metal_variant_part_ring_id) {
                        input = document.createElement('input');
                        name = key + '[' + i + ']' + '[metal_variant_part_ring_id]';
                        input.name = name;
                        input.value = config[key][i].metal_variant_part_ring_id;
                        form_container.appendChild(input);
                    }
                    if (config[key][i].type_stone_variant_part_ring_id) {
                        input = document.createElement('input');
                        name = key + '[' + i + ']' + '[type_stone_variant_part_ring_id]';
                        input.name = name;
                        input.value = config[key][i].type_stone_variant_part_ring_id;
                        form_container.appendChild(input);
                    }
                }
            }
        }

        document.body.style.overflow = 'auto';

        document.getElementById("order-form").submit();
    }

    init() {
        this.changePrice(this.data.price);
        if (this.type === 3) {
            document.getElementById('size-block').style.display = 'none';
            document.getElementById('sizes-slider').style.display = 'none';
        } else {
            this.showSizes(this.data.sizes);
        }
        this.initRaycaster();
        this.orderEvent();
    }

}