import hand from '../../assets/model/hand/hand.obj';
import hand_texture from '../../assets/textures/hand/hand.png';
import neck from '../../assets/model/neck/neck_model.obj';

const skins = [
    {
        color: {r: 1, g: 1, b: 1},
        emissive: {r: 0.109, g: 0.109, b: 0.109}
    },
    {
        color: {r: 1, g: 1, b: 1},
        emissive: {r: 0, g: 0, b: 0}
    },
    {
        color: {r: 0.95, g: 0.95, b: 0.95},
        emissive: {r: 0, g: 0, b: 0}
    },
    {
        color: {r: 0.87, g: 0.87, b: 0.87},
        emissive: {r: 0, g: 0, b: 0}
    },
    {
        color: {r: 0.8, g: 0.8, b: 0.8},
        emissive: {r: 0, g: 0, b: 0}
    },
    {
        color: {r: 0.7, g: 0.7, b: 0.7},
        emissive: {r: 0, g: 0, b: 0}
    },
    {
        color: {r: 0.6, g: 0.6, b: 0.6},
        emissive: {r: 0, g: 0, b: 0}
    },
    {
        color: {r: 0.52, g: 0.52, b: 0.52},
        emissive: {r: 0, g: 0, b: 0}
    }

];

export default class Hand {
    constructor(viewer, loading) {
        this.viewer = viewer;
        this.loading = loading;
        this.loader = new THREE.OBJLoader();
        this.textureLoader = new THREE.TextureLoader();

        this.hand = 'no_hand';
        this.slider_container = document.getElementById("slider_container");

        // this.showHand();
    }

    skinControl() {
        let slider = document.getElementById("myRange");
        this.slider_container.style.display = 'block';

        slider.oninput = (e) => {
            const value = parseInt(e.target.value) - 1;

            this.hand.material.color.r = skins[value].color.r;
            this.hand.material.color.g = skins[value].color.g;
            this.hand.material.color.b = skins[value].color.b;

            this.hand.material.emissive.r = skins[value].emissive.r;
            this.hand.material.emissive.g = skins[value].emissive.g;
            this.hand.material.emissive.b = skins[value].emissive.b;

        }
    }

    showHand() {
        if (this.hand === 'no_hand') {
            this.loading.show();
            if (this.type !== 3) { // necklace
                this.textureLoader.load(hand_texture, (texture) => {
                    this.loader.load(hand, (obj) => {
                        const hand = obj.children[0];
                        hand.scale.x = 0.08;
                        hand.scale.y = 0.08;
                        hand.scale.z = 0.08;

                        hand.material.map = texture;
                        hand.name = 'hand';

                        hand.material.emissive.r = skins[0].emissive.r;
                        hand.material.emissive.g = skins[0].emissive.g;
                        hand.material.emissive.b = skins[0].emissive.b;

                        this.hand = hand;
                        this.viewer.scene.add(hand);
                        this.loading.hide();

                        this.skinControl();
                        this.setPositions();
                    });

                });
            } else {
                this.loader.load(neck, (neck_obj) => {
                    const neckModel = neck_obj.children[0];
                    neckModel.name = 'hand';
                    this.loading.hide();
                    this.hand = neckModel;
                    this.viewer.scene.add(neckModel);
                    this.hand.scale.set(0.45, 0.45, 0.45);
                    this.hand.position.y = 8;

                    this.setPositions();
                });
            }
        } else {
            this.setPositions();
        }
    }

    setPositions() {
        this.viewer.handState = true;
        if (this.type !== 3) { //necklace
            this.slider_container.style.display = 'block';
            this.hand.position.set(-2, 10, -30);
            this.hand.rotation.set(0, -3.5, 0);
            this.viewer.controls.maxPolarAngle = 1.1;
            this.viewer.controls.minPolarAngle = 0.45;
            this.viewer.controls.maxAzimuthAngle = -3.14;
            this.viewer.controls.minAzimuthAngle = -3.14;
            this.viewer.controls.rotateSpeed = 0.05;

            this.viewer.camera.position.set(-4.35, 76, -71);
            this.viewer.objContainer.position.set(-0.7, 7.22, -11.4);
            this.viewer.objContainer.scale.set(0.25, 0.25, 0.25);
            this.viewer.objContainer.rotation.y = 0.16;
            this.hand.visible = true;
        } else {
            this.hand.visible = true;
            this.viewer.controls.rotateSpeed = 0.1;
            this.viewer.controls.minPolarAngle = 1.1;
            this.viewer.controls.maxPolarAngle = 1.8;
            // this.viewer.controls.maxAzimuthAngle = 0.7;
            // this.viewer.controls.minAzimuthAngle = -0.7;
            this.viewer.controls.maxAzimuthAngle = 0;
            this.viewer.controls.minAzimuthAngle = 0;
            this.viewer.objContainer.scale.set(0.7, 0.7, 0.7);
            this.viewer.controls.maxDistance = 290;
            this.viewer.controls.minDistance = 290;
            this.viewer.objContainer.rotation.x = -0.07;
        }
    }

    hideHand() {
        this.slider_container.style.display = 'none';

        this.viewer.controls.maxPolarAngle = Math.PI;
        this.viewer.controls.minPolarAngle = 0;
        this.viewer.controls.maxAzimuthAngle = Infinity;
        this.viewer.controls.minAzimuthAngle = -Infinity;
        this.viewer.controls.rotateSpeed = 1;
        this.viewer.controls.maxDistance = 110;
        this.viewer.controls.minDistance = 60;

        this.viewer.scene.rotation.x = 0;
        this.viewer.camera.position.set(0, 30, 62);
        this.viewer.objContainer.position.set(0, 0, 0);
        this.viewer.objContainer.scale.set(1, 1, 1);
        this.viewer.objContainer.rotation.y = 0;
        this.viewer.objContainer.rotation.x = 0;
        this.hand.visible = false;
        this.viewer.handState = false;
    }
}