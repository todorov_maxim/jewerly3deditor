import seventeen from '../../assets/model/engrav/17_2.obj';
import ninghteen from '../../assets/model/engrav/19_2.obj';

export default class ObjLoader {
    constructor(viewer, ringMaterial, loading) {
        this.loading = loading;
        this.viewer = viewer;
        this.ringMaterial = ringMaterial;
        this.loader = new THREE.OBJLoader();
        this.neededParts = 0;

        this.loadEngrav();

        this.check = 0;

    }

    loadPart(url, container, img_material, key) {

        if (url.indexOf('drc') !== -1) {
            // Configure and create Draco decoder.
            THREE.DRACOLoader.setDecoderPath('./libs/draco/');
            THREE.DRACOLoader.setDecoderConfig({type: 'js'});
            var dracoLoader = new THREE.DRACOLoader();
            this.ringMaterial.createMaterial(img_material, (material) => {

                dracoLoader.load(url, (geometry) => {

                    geometry.computeVertexNormals();
                    var obj = new THREE.Mesh(geometry, material);


                    let part = obj;
                    part.key = key;
                    part.material = material;
                    part.name = 'part';
                    container.add(part);

                    if (this.neededParts <= 1) {
                        this.loading.hide();
                    }
                    this.neededParts--;


                    if (!this.check) {
                        this.check = true;
                        container.parent.add(this.viewer.engrav);
                    }


                    // Release decoder resources.
                    THREE.DRACOLoader.releaseDecoderModule();

                });

            });
        } else {
            if (img_material === '/') {
                this.loader.load(url, (obj) => {
                    let part = obj.children[0];
                    part.key = key;
                    part.visible = false;
                    part.name = 'part';
                    container.add(part);

                    if (this.neededParts <= 1) {
                        this.loading.hide();
                    }
                    this.neededParts--;


                    if (!this.check) {
                        this.check = true;
                        container.parent.add(this.viewer.engrav);
                    }
                });
            } else {
                this.ringMaterial.createMaterial(img_material, (material) => {
                    this.loader.load(url, (obj) => {
                        let part = obj.children[0];
                        part.key = key;
                        part.material = material;
                        part.name = 'part';
                        container.add(part);

                        if (this.neededParts <= 1) {
                            this.loading.hide();
                        }
                        this.neededParts--;


                        if (!this.check) {
                            this.check = true;
                            container.parent.add(this.viewer.engrav);
                        }
                    });

                });
            }
        }
    }

    loadNull() {
        if (this.neededParts <= 1) {
            this.loading.hide();
        }
        this.neededParts--;
    }

    loadEngrav() {
        this.loader.load(window.location.href.indexOf('adara') !== -1 ? ninghteen : seventeen, (obj) => {
            const engrav = obj.children[0];
            engrav.position.y = window.location.href.indexOf('adara') !== -1 ? 0.02 : -0.65;
            engrav.material.side = 2;
            engrav.material.transparent = true;
            engrav.name = 'gravirovka';
            engrav.visible = false;
            this.viewer.engrav = engrav;
        });

        // this.loader.load(seventeen, (obj)=>{
        //     const engrav = obj.children[0];
        //     engrav.material.side = 2;
        //     engrav.material.transparent = true;
        //     engrav.name = 'gravirovka';
        // });
    }

}