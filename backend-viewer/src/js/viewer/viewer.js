export default class Viewer{
	constructor(){
		this.init();
	}

	init(){
		// CONTAINER
		this.container = document.createElement( 'div' );
		this.container.id = 'threeJS';
		document.body.appendChild( this.container );

		// CAMERA
		window.camera = this.camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 2000 );
		this.camera.position.set(0, 28, 41);

		// SCENE
		this.scene = new THREE.Scene();

		// RENDERER
		this.renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
		this.renderer.setPixelRatio( window.devicePixelRatio );
		this.renderer.setSize( window.innerWidth, window.innerHeight );
		this.renderer.shadowMap.enabled = true;

		// LIGHT
		this.initLight();

		// CONTROLS
		this.initControls();

		// ADD ON SCENE
		this.container.appendChild( this.renderer.domElement );

		// RESIZE EVENT
		window.addEventListener('resize', this.onWindowResize.bind(this));

		this.animate()
	}

	initControls(){
		this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
		this.controls.maxDistance = 110;
		this.controls.minDistance = 40;
		this.controls.enableDamping = true;
		this.controls.dampingSpeed = 0.05;
		this.controls.enablePan = false;
		this.controls.rotateSpeed = 0.5;
	}

	initLight(){
		let light = new THREE.HemisphereLight( 0xffffff, 0x444444 );
		light.name = 'HemisphereLight';
		light.position.set( 0, 300, 0 );
		this.scene.add( light );
	}

	onWindowResize() {
		this.camera.aspect = window.innerWidth / window.innerHeight;
		this.camera.updateProjectionMatrix();
		this.renderer.setSize( window.innerWidth, window.innerHeight );
	}

	animate(){
		requestAnimationFrame( ()=>{
			this.animate()
		});

		this.controls.update();
		this.renderer.render( this.scene, this.camera );
	}
}