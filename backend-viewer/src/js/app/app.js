import Viewer from '../viewer/viewer';
import Loader from '../loaders/loader';
import Loading from './loading/loading';
export default class App{
    constructor(){
        this.viewer = new Viewer();
        this.loading = new Loading(this.viewer.container);
        this.loader = new Loader(this.viewer, this.loading);
        this.init();
    }

    init(){
        const src = window.location.search.split('?model_src=')[1];
        this.loader.loadModel(src);
    }
}