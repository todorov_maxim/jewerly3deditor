export default class CustomRotate{
	constructor(object, container){
		this.object = object;
		this.container = container;

		this.mouse = {
			x: 0,
			y: 0
		};

		this.events();
	}

	setObject(object){
		this.object = object;
		this.objectRotation = {
			x: this.object.rotation.y,
			y: this.object.rotation.z
		};
	}

	events(){

		// Desktop events
		this.container.addEventListener('mousedown', (event)=>{
			this.mouse.x = event.clientX;
			this.mouse.y = event.clientY;

			this.rotationStart();
		});

		document.addEventListener('mousemove', (event)=>{
			if(this.mouse.x !== 0){
				let xDirection = event.x - this.mouse.x;
				let yDirection = event.y - this.mouse.y;

				this.rotate(xDirection, yDirection);
			}
		});

		document.addEventListener('mouseup', ()=>{
			this.mouse = {
				x: 0,
				y: 0
			};
		});


		//Mobile events
		this.container.addEventListener('touchstart', (event)=>{
			this.mouse.x = event.touches[0].clientX;
			this.mouse.y = event.touches[0].clientY;
			this.rotationStart();
		});

		document.addEventListener('touchmove', (event)=>{
			if(this.mouse.x !== 0){
				let xDirection = event.touches[0].clientX - this.mouse.x;
				let yDirection = event.touches[0].clientY - this.mouse.y;

				this.rotate(xDirection, yDirection);
			}
		});

		document.addEventListener('touchend', ()=>{
			this.mouse = {
				x: 0,
				y: 0
			};
		});
	}

	rotationStart(){
		this.objectRotation = {
			x: this.object.rotation.y,
			y: this.object.rotation.z
		};
	}

	rotate(xDirection, yDirection){
		let xRotate = xDirection / this.container.offsetWidth * Math.PI;
		let yRotate = yDirection / this.container.offsetHeight * Math.PI;

		this.object.rotation.y = this.objectRotation.x + xRotate;
		// this.object.rotation.x = this.objectRotation.y + yRotate;
	}

}