import App from './app/app';


document.addEventListener("DOMContentLoaded", () => {
    if (!window.location.href.includes('create')) {
        document.documentElement.style.overflow = 'hidden';
        document.body.style.overflow = 'hidden';
    }
    new App();
});
