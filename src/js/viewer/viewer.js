export default class Viewer {
    constructor() {
        this.init();

        this.IsIBLDataRead = false;
        this.bObjectLoaded = false;
        this.bRingLoaded = false;
        this.diamondArray = [];
        this.diamondPrototypeArray = [];

        this.autoRotateIndex = 0;
        this.handState = false;

    }

    init() {
        // CONTAINER
        this.container = document.getElementById('threeJS');

        // CAMERA
        this.camera = new THREE.PerspectiveCamera(45, this.container.offsetWidth / this.container.offsetHeight, 0.3, 1000);
        this.camera.position.y = 30;
        this.camera.position.z = 75;


        // SCENE
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0xfaf7f8);
        this.scene.add(this.camera);
        window.scene = this.scene;

        // 3d-Container
        this.objContainer = new THREE.Object3D();
        this.objContainer.name = 'objContainer';
        this.objContainer.position.y = -3;
        this.scene.add(this.objContainer);

        // RENDERER
        this.renderer = new THREE.WebGLRenderer({antialias: true, alpha: false, precision: 'highp'});
        this.renderer.setPixelRatio(devicePixelRatio);
        this.renderer.setSize(this.container.offsetWidth, this.container.offsetHeight);
        this.renderer.shadowMap.enabled = true;


        this.renderScene = new THREE.RenderPass(this.scene, this.camera);
        this.renderScene.clearColor = 0xffffff;
        this.renderScene.clear = true;
        this.effectFXAA = new THREE.ShaderPass(THREE.FXAAShader);
        this.effectFXAA.uniforms['resolution'].value.set(1 / (this.container.offsetWidth * window.devicePixelRatio), 1 / (this.container.offsetHeight * window.devicePixelRatio));
        this.effectFXAA.renderToScreen = true;

        this.composer = new THREE.EffectComposer(this.renderer);
        this.composer.setSize(this.container.offsetWidth * window.devicePixelRatio, this.container.offsetHeight * window.devicePixelRatio);
        this.composer.addPass(this.renderScene);
        this.composer.addPass(this.effectFXAA);

        // GUI
        // var gui = new dat.GUI();
        //
        // var Configuration = function () {
        // 	this.color = "#ffffff";
        // };
        // let conf = new Configuration();
        //
        // let controlador = gui.addColor(conf, 'color');
        // controlador.onChange( (colorValue) => {
        // 	//the return value by the chooser is like as: #ffff
        // 	colorValue = colorValue.replace('#', '');
        // 	function hexToRgb(hex) {
        // 		let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        // 		return result ? {
        // 			r: parseInt(result[1], 16),
        // 			g: parseInt(result[2], 16),
        // 			b: parseInt(result[3], 16)
        // 		} : null;
        // 	}
        //
        // 	let rgba = hexToRgb(colorValue);
        // 	for (let i = 0; i < this.diamondArray.length; i++) {
        // 		let absorbption = this.diamondArray[i].material.uniforms["Absorbption"].value;
        // 		absorbption.x = 1.0 - rgba.r / 255;
        // 		absorbption.y = 1.0 - rgba.g / 255;
        // 		absorbption.z = 1.0 - rgba.b / 255;
        // 	}
        // });
        // gui.open();

        // LIGHT
        this.initLight();

        // CONTROLS
        this.initControls();

        // ADD ON SCENE
        this.container.appendChild(this.renderer.domElement);

        // RESIZE EVENT
        window.addEventListener('resize', this.onWindowResize.bind(this));

        setTimeout(() => {
            this.onWindowResize();
        }, 1000);

        this.animate()
    }

    initControls() {
        this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
        this.controls.maxDistance = 110;
        this.controls.minDistance = 60;
        this.controls.enableDamping = true;
        this.controls.dampingSpeed = 0.05;
        this.controls.enablePan = false;
        this.controls.rotateSpeed = 0.5;
        this.controls.autoRotate = true;
        this.controls.autoRotateSpeed = 1;


        document.addEventListener('click', ()=>{
            this.controls.autoRotate = false;
            this.autoRotateIndex = 0;
        });
        document.addEventListener('touchstart', ()=>{
            this.controls.autoRotate = false;
            this.autoRotateIndex = 0;
        });


        window.controls = this.controls;
    }

    initLight() {
        let light = new THREE.HemisphereLight(0xffffff, 0x444444);
        light.name = 'HemisphereLight';
        light.position.set(0, 300, 0);
        this.scene.add(light);
    }

    onWindowResize() {
        this.camera.aspect = this.container.offsetWidth / this.container.offsetHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(this.container.offsetWidth, this.container.offsetHeight);
        // this.effectFXAA.uniforms['resolution'].value.set(1 / this.container.offsetWidth, 1 / this.container.offsetHeight);
    }

    animate() {
        requestAnimationFrame(() => {
            this.animate()
        });

        this.autoRotateIndex++;
        if(this.autoRotateIndex > 420 && !this.handState){
            this.controls.autoRotate = true;
        }

        this.controls.update();
        this.renderer.render(this.scene, this.camera);

        if (this.IsIBLDataRead && this.bObjectLoaded && this.bRingLoaded) {

            for (var i = 0; i < this.diamondArray.length; i++) {
                this.diamondArray[i].alignSparklesWithCamera(this.camera);
            }

            for (var i = 0; i < this.diamondPrototypeArray.length; i++) {
                // if(!this.diamondPrototypeArray.prepared) {
                if (!this.diamondPrototypeArray[i].count || this.diamondPrototypeArray[i].count < 1) {
                    this.diamondPrototypeArray[i].count++;
                    this.diamondPrototypeArray[i].prepareNormalsCubeMap(this.renderer);
                    // this.diamondPrototypeArray.prepared = true;
                    // }
                }
            }
            for (var i = 0; i < this.diamondArray.length; i++) {
                this.diamondArray[i].applyTransform();
            }
        }

    }
}
